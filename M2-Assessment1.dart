class Tester {
  var appname;
  var sector;
  var developer;
  var yearwon;
  //3.A
  showTesterInfo() {
    print(
        '$appname won in the category of $sector, in the year $yearwon, it was developed by $developer');
  }

  //3.B
  showTesterAllCaps(appname) {
    print('App Name in all caps: $appname');
  }
}

void main() {
  //Section 1
  var MTN1 = {"name": 'Ryan', "favorite_app": 'Twitter', "city": 'Cape Town'};
  print(MTN1['name']);
  print(MTN1['favorite_app']);
  print(MTN1['city']);
  print('');

  //Section 2
  var arr = [
    "FNB 2012",
    "Snapscan 2013",
    "Live Inspects 2014",
    "WumDrop 2015",
    "Domestly 2016",
    "Shyft 2017",
    "Khula Ecosystem 2018",
    "Naked Insurance 2019",
    "Easy Equities 2020",
    "EdTech 2021"
  ];
  //A Sort and print the apps by name
  arr.sort();
  print(arr);
  print('');
  //B Print the winning app of 2017 and 2018
  for (var years in arr) {
    if (years.contains('2017')) {
      print(years);
    }
    if (years.contains('2018')) {
      print(years);
    }
  }
  print('');
  //C Print the total number of app in the array
  print(arr.length);
  print('');

  //Section 3
  var test = new Tester();
  test.appname = 'FNB Banking';
  test.sector = 'Best Consumer Solution';
  test.developer = 'FNB';
  test.yearwon = '2012';
  //3.A
  test.showTesterInfo();
  //3.B
  test.showTesterAllCaps(test.appname.toUpperCase());
}
